import { Controller } from "stimulus";

export default class extends Controller {
  static targets = [ "link" ]

  open(e) {
    
    e.target.blur();
    
    const path_name = this.linkTarget.href.replace(/^.*\/\/[^\/]+/, '');
    if (path_name != window.location.pathname) {
        history.pushState({}, path_name, path_name);
    };

    const event = new CustomEvent("link:click", { bubbles: true, cancelable: true });
    document.dispatchEvent(event);
  }
}