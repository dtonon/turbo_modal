import { Controller } from "stimulus";

export default class extends Controller {
    static values = {
        url: String,
        match: String
    }

    connect() {
        fetch(this.urlValue)
            .then(res => res.text())
            .then(res => {document.getElementsByTagName('body')[0].innerHTML = res;})
            .then( res =>  {
                var path_name = window.location.pathname;
                if (path_name.match(new RegExp(this.matchValue))) {
                    var link = document.querySelectorAll("a[href='" + path_name + "']");
                    link[0].click();
                }
            })
    }
}