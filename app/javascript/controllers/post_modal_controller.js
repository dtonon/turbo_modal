import { Modal } from "tailwindcss-stimulus-components";

export default class extends Modal {
  static targets = ['container'];
  static values = {
    'indexUrl': String,
    'focusField': String
  }

  click_handler = e =>  {
    this.open(e);
  }

  close_handler = e => {
    this.close(e)
  }

  connect() {
    super.connect();
    document.addEventListener("link:click", this.click_handler);
    document.addEventListener("modal:close", this.close_handler);
  }

  open(e) {
    if (this.preventDefaultActionOpening) {
      e.preventDefault();
    }

    // Lock the scroll and save current scroll position
    this.lockScroll();

    // Unhide the modal
    document.addEventListener("postForm:load", () => {
      this.containerTarget.classList.remove(this.toggleClass);
      document.getElementById(this.focusFieldValue).focus();
    });

    // Insert the background
    if (!this.data.get("disable-backdrop")) {
      var bg_check = document.querySelector(`#${this.backgroundId}`);
      if (!bg_check) {
        document.body.insertAdjacentHTML('beforeend', this.backgroundHtml);
        this.background = document.querySelector(`#${this.backgroundId}`);
      };
    }


  }

  close(e){
    e.preventDefault();
    if (window.location.pathname != this.indexUrlValue) {history.pushState({}, this.indexUrlValue, this.indexUrlValue)};
    super.close();
  }
  
  close_if_ok(e){
    if(e.detail["success"] == true){
      this.close(e);
    }
  }

  disconnect(){
    document.removeEventListener("link:click", this.click_handler);
    document.removeEventListener("modal:close", this.close_handler);
    this.background = document.querySelector(`#${this.backgroundId}`);
    if (this.background){this.background.remove()};
  }

}