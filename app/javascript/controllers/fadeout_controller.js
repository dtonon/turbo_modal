import { Controller } from "stimulus"

export default class extends Controller {

  connect() {
    this.element.classList.add('fadeout');
    setTimeout(function(el){ 
      el.classList.add('hidden');
    }, 700, this.element);
  };
  
}