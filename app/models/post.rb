class Post < ApplicationRecord
  validates :title, :body, presence: true

  def tag_id
    "post_#{self.id}"
  end

end
